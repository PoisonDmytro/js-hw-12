document.addEventListener("keyup",function(event){
    let key = event.key;
    if(key.length == 1){
        key = key.toUpperCase();
    }
    let oldClick = document.querySelector('.active');
    if(oldClick != null){
        oldClick.classList.remove('active');
    }
    document.querySelector(`#${key}`).classList.add("active");
    
})
let btnList = document.querySelectorAll('#btnList > button')
for(btn of btnList){
    btn.id = btn.textContent;
}

